from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Cundinas(models.Model):

    numero=models.CharField(max_length=15)
    manager=models.ForeignKey(User,on_delete=models.DO_NOTHING,default=None)
    cundina_name =models.CharField(max_length=15)
    amount=models.IntegerField()
    number_of_persons_involved=models.CharField(max_length=15)

    def __str__(self):
        return self.cundina_name


class Persona(models.Model):

    numero=models.CharField(max_length=15)
    cundina_name =models.ForeignKey(Cundinas,on_delete=models.DO_NOTHING,default=None)
    contributor_1=models.CharField(max_length=15)
    contributor_2=models.CharField(max_length=15)
    contributor_3=models.CharField(max_length=15)
    contributor_4=models.CharField(max_length=15)
    contributor_5=models.CharField(max_length=15)

    def __str__(self):
        return str(self.cundina_name) if self.cundina_name else ''

class Managers(models.Model):

    manager=models.ForeignKey(User,on_delete=models.DO_NOTHING,default=None)


    def __str__(self):
        return self.manager


def get_absolute_url(self):
        return reverse('arte_edit', kwargs={'pk': self.pk})
