from django.contrib import admin

# Register your models here.
from .models import Cundinas, Persona


admin.site.register(Cundinas)


admin.site.register(Persona)
