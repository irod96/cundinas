from django import forms
from .models import Cundinas,Persona,Managers

class Cform(forms.ModelForm):
    class Meta:
     model=Cundinas
     fields= [
      "id",
      "manager",
      "cundina_name",
      "amount",
      "number_of_persons_involved",
     ]

class Cform2(forms.ModelForm):
     class Meta:
      model=Persona
      fields= [
      "id",
      "cundina_name",
      "contributor_1",
      "contributor_2",
      "contributor_3",
      "contributor_4",
      "contributor_5",
      ]


class Cform3(forms.ModelForm):
     class Meta:
      model=Managers
      fields= [
       "manager"
      ]
